from flask import Flask

app = Flask (__name__)

@app.route("/")
def index():
    return '<h1>What up!</h1>'

@app.route("/information")
def info():
    return 'This the info about this company.'

# dynamic routing
@app.route("/distro/<name>")
def distro(name):
    return "<h2>This is a page for {}.</h2>".format(name)

if __name__ == '__main__':
    app.run(port=5000, debug=True)