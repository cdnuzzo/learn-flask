# create entries into the tables

from models import db,Puppy,Owner,Toy

# create 2 puppies

rufus = Puppy('Rufus')
fido = Puppy('Fido')

# add puppies to db
db.session.add_all([rufus,fido])
db.session.commit()

# check it
print(Puppy.query.all())

rufus = Puppy.query.filter_by(name='Rufus').first()
print(rufus)

# create owner object
craig = Owner('Craig',rufus.id)

# give Rufus toys
toy1 = Toy('Chew Toy',rufus.id)
toy2 = Toy('Ball',rufus.id)

db.session.add_all([craig,toy1,toy2])
db.session.commit()

#grab rufus after those additions
rufus = Puppy.query.filter_by(name='Rufus').first()
print(rufus)

rufus.report_toys()

